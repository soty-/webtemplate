﻿using System;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Services.Validators;

namespace WebApp.Attributes
{
    public class ExceptionHandlerAttribute : Attribute, IExceptionFilter
    {
        public void OnException(ExceptionContext filterContext)
        {
            if (filterContext.Exception != null && filterContext.Exception.GetType() == typeof(ValidationException))
            {
                filterContext.HttpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }

            if (filterContext.Exception != null && filterContext.Exception.GetType() == typeof(NotFoundException))
            {
                filterContext.HttpContext.Response.StatusCode = (int)HttpStatusCode.NotFound;
            }

            filterContext.ExceptionHandled = true;
        }
    }
}
