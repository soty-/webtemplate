﻿using Microsoft.AspNetCore.Mvc;
using Services.Dtos;
using Services;

namespace WebApp.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        private readonly IMyService service;

        public ValuesController(IMyService service)
        {
            this.service = service;
        }
        
        [HttpGet]
        public ActionResult Get()
        {
            return Json(service.GetAll());
        }
        
        [HttpGet("{id}")]
        public ActionResult Get(int id)
        {
            return Json(service.GetById(id));
        }
        
        [HttpPost]
        public void Post([FromBody]string value)
        {
            service.Add(new TestDto() { TestProp = value });
        }
        
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
            service.Update(new TestDto() { Id = id, TestProp = value });
        }
        
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            service.Remove(id);
        }
    }
}
