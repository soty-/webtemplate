﻿using System.Linq;
using AutoMapper;
using DAL.Entities;
using DAL.Interfaces;
using Moq;
using NUnit.Framework;
using Services.Validators;

namespace Services.Test
{
    [TestFixture]
    public class MyServiceTest : TestBase
    {
        private MyService sut;
        private Mock<IMyRepository> myRepositoryMock;
        private Mock<ITestDtoValidator> validatorMock;

        [SetUp]
        public void SetUp()
        {
            SetupMocks();
            sut = new MyService(Mapper.Instance, myRepositoryMock.Object, validatorMock.Object);
        }

        [Test]
        public void ShouldGetById()
        {
            // act
            var result = sut.GetById(1);

            // assert
            Assert.AreEqual(1, result.Id);
            Assert.AreEqual("test value 1", result.TestProp);
        }

        [Test]
        public void ShouldRemove()
        {
            // act
            sut.Remove(1);

            // assert
            myRepositoryMock.Verify(x => x.Remove(1), Times.Once);
        }

        [Test]
        public void ShouldFailForRemovingNotExistingItem()
        {
            // arrange
            validatorMock.Setup(x => x.Exists(3)).Throws(new ValidationException("Entity does not exist"));

            // assert
            Assert.Throws(typeof(ValidationException), () => sut.Remove(3));
        }

        private void SetupMocks()
        {
            myRepositoryMock = new Mock<IMyRepository>();
            validatorMock = new Mock<ITestDtoValidator>();

            var entities = new[]
            {
                new TestEntity { Id = 1, Value = "test value 1" },
                new TestEntity { Id = 2, Value = "test value 2" },
                new TestEntity { Id = 3, Value = "test value 3" },
                new TestEntity { Id = 4, Value = "test value 4" },
            }.ToList();

            myRepositoryMock.Setup(x => x.GetAll()).Returns(entities.AsQueryable());
        }
    }
}
