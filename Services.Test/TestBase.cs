﻿using AutoMapper;
using Services.AutoMapperProfiles;

namespace Services.Test
{
    public abstract class TestBase
    {
        public TestBase()
        {
            Mapper.Initialize(cfg => cfg.AddProfile<TestProfile>());
        }
    }
}
