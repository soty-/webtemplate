﻿using System.ComponentModel.DataAnnotations;

namespace DAL.Entities
{
    public class TestEntity
    {
        [Required]
        public int Id { get; set; }

        [Required]
        [MinLength(5)]
        public string Value { get; set; }
    }
}
