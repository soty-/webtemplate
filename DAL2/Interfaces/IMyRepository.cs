﻿using DAL.Entities;
using System.Linq;

namespace DAL.Interfaces
{
    public interface IMyRepository
    {
        IQueryable<TestEntity> GetAll();

        TestEntity Add(TestEntity entity);

        TestEntity Update(TestEntity entity);

        void Remove(int id);
    }
} 
