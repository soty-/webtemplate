﻿using DAL.Entities;
using DAL.Interfaces;
using System.Linq;
using System.Collections.Generic;

namespace DAL.Repositories
{
    public class MyRepository : IMyRepository
    {
        private List<TestEntity> Values = new[]
            {
                new TestEntity { Id = 1, Value = "test value 1" },
                new TestEntity { Id = 2, Value = "test value 2" },
                new TestEntity { Id = 3, Value = "test value 3" },
                new TestEntity { Id = 4, Value = "test value 4" },
            }.ToList();

        public IQueryable<TestEntity> GetAll()
        {
            return Values.AsQueryable();
        }

        public TestEntity Add(TestEntity entity)
        {
            entity.Id = Values.Max(e => e.Id) + 1;
            Values.Add(entity);

            return entity;
        }

        public TestEntity Update(TestEntity entity)
        {
            var oldValue = Values.FirstOrDefault(e => e.Id == entity.Id);
            oldValue.Value = entity.Value;

            return oldValue;
        }

        public void Remove(int id)
        {
            Values.Remove(Values.FirstOrDefault(e => e.Id == id));
        }
    }
}
