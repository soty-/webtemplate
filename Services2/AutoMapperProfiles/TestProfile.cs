﻿using AutoMapper;
using DAL.Entities;
using Services.Dtos;

namespace Services.AutoMapperProfiles
{
    public class TestProfile : Profile
    {
        public TestProfile()
        {
            CreateMap<TestDto, Test2Dto>()
                .ForMember(dst => dst.Test2Prop, src => src.MapFrom(x => x.TestProp))
                .ReverseMap();

            CreateMap<TestEntity, TestDto>()
                .ForMember(dst => dst.TestProp, src => src.MapFrom(x => x.Value))
                .ReverseMap();
        }
    }
}
