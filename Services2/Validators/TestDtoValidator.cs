﻿using System.Linq;
using DAL.Interfaces;
using Services.Dtos;

namespace Services.Validators
{
    public sealed class TestDtoValidator : ITestDtoValidator
    {
        private readonly IMyRepository testRepository;

        public TestDtoValidator(IMyRepository testRepository)
        {
            this.testRepository = testRepository;
        }

        public void Validate(TestDto dto)
        {
            if (string.IsNullOrEmpty(dto.TestProp))
            {
                throw new ValidationException("Test prop must have value");
            }

            if (dto.TestProp.Length < 5)
            {
                throw new ValidationException("Test prop must have at east 5 characters");
            }
        }

        public void Exists(int id)
        {
            if (!testRepository.GetAll().Any(x => x.Id == id))
            {
                throw new NotFoundException("Entity does not exist");
            }
        }
    }
}
