﻿using Services.Dtos;

namespace Services.Validators
{
    public interface ITestDtoValidator
    {
        void Validate(TestDto dto);

        void Exists(int id);
    }
}