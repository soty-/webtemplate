﻿using Services.Dtos;
using System.Collections.Generic;

namespace Services
{
    public interface IMyService
    {
        IEnumerable<TestDto> GetAll();

        TestDto GetById(int id);

        TestDto Add(TestDto dto);

        TestDto Update(TestDto dto);

        void Remove(int id);
    }
}
