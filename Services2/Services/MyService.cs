﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using DAL.Entities;
using DAL.Interfaces;
using Services.Dtos;
using Services.Validators;

namespace Services
{
    public class MyService : IMyService
    {
        private readonly IMapper mapper;
        private readonly IMyRepository testRepository;
        private readonly ITestDtoValidator testDtoValidator;

        public MyService(
            IMapper mapper,
            IMyRepository testRepository,
            ITestDtoValidator testDtoValidator)
        {
            this.mapper = mapper;
            this.testRepository = testRepository;
            this.testDtoValidator = testDtoValidator;
        }

        public TestDto Add(TestDto dto)
        {
            testDtoValidator.Validate(dto);

            return mapper.Map<TestDto>(testRepository.Add(mapper.Map<TestEntity>(dto)));
        }

        public IEnumerable<TestDto> GetAll()
        {
            var entities = testRepository.GetAll();
            return mapper.Map<TestDto[]>(entities);
        }

        public TestDto GetById(int id)
        {
            var entity = testRepository.GetAll().FirstOrDefault(x => x.Id == id);
            return mapper.Map<TestDto>(entity);
        }

        public void Remove(int id)
        {
            testDtoValidator.Exists(id);
            testRepository.Remove(id);
        }

        public TestDto Update(TestDto dto)
        {
            testDtoValidator.Exists(dto.Id);
            return mapper.Map<TestDto>(testRepository.Update(mapper.Map<TestEntity>(dto)));
        }
    }
}
